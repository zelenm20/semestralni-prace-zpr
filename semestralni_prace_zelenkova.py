import matplotlib.pyplot as plt
from tinydb import TinyDB as db
from tinydb import Query as qu
import keyboard as kb
import datetime as dt
import calendar

def testifnumber(que):
    while (True):
        subj = input(que)
        try:
            subj = int(subj)
            return subj
        except:
            print('Input must be a number')

def checkdate():
    while(True):
        try:
            while(True):
                year = int(input('Enter a year: '))
                if year < 1900 or year > dt.datetime.now().year: 
                    continue
                else:
                    break
            while(True):
                month = int(input('Enter a month (1-12): '))
                if year == dt.datetime.now().year:   
                    if month > dt.datetime.now().month: 
                        continue
                if month < 1 or month > 12: 
                    continue
                break
            while(True):
                day = int(input('Enter a day: '))
                if month == dt.datetime.now().month and year == dt.datetime.now().year:   
                    if day > dt.datetime.now().day or day < 1: continue
                if month == 4 or month == 6 or month == 9 or month == 11:
                    if day > 30: 
                        continue
                elif month == 2:
                    if calendar.isleap(year):
                        if day > 29:
                            continue
                    else:
                        if day > 28:
                            continue
                else:
                    if day > 31:
                        continue
                break
            
            d = dt.date(year, month, day)
            return d
        except:
            print('Input not valid.')

balance = db('balance.json')
logs = qu()

print('The database contains placeholder data. If you want to delete all saved data press \'d\'. If you don\'t wish to delete them press \'k\'.')
deleteData = False
while(True):
    if kb.is_pressed('d'): 
        deleteData = True
        break
    if kb.is_pressed('k'):
        deleteData = False
        break

if deleteData == True:
    balance.truncate()

print('Press \'c\' if you want to change your balance or press \'n\' if you don\'t.')
adding = True
while(True):
    if kb.is_pressed('c'): 
        adding = True
        break
    if kb.is_pressed('n'):
        adding = False
        break

while(adding):
    print('Press \'i\' if you want to add an  income or press \'e\' if you want to add a new expense.')
    change = ''
    while(True):
        if kb.is_pressed('i'):
            print('Adding income')
            change = 'income'
            break
        if kb.is_pressed('e'):
            print('Adding an expense')
            change = 'expense'
            break
    
    category = input('What is the category? ')
    amount = testifnumber('What is the amount? ')
    print('Press \'t\' if you want to save it under the current date, press \'c\' if you want to enter a custom date')
    
    dateAdded = dt.datetime.now().date().strftime('%Y-%m-%dnnnny')
    while(True):
        if kb.is_pressed('t'):
            break
        if kb.is_pressed('c'):
            print('Enter a custom date.')
            dateAdded = checkdate()
            break
    
    balance.insert({'change': change, 'category': category, 'amount': amount, 'date': str(dateAdded)})
    
    print('Press \'y\' if you want to add more, \'n\' if not.')
    while(True):
        if kb.is_pressed('y'): 
            adding = True
            break
        if kb.is_pressed('n'): 
            adding = False
            break
for info in balance:
    print('{change}\n category: {category}\n amount: {amount}\n date: {date}'.format(**info))

all_rows = balance.all()

all_dates = list({row['date'] for row in all_rows})
all_dates.sort()

dates = []
amount = []
for d in all_dates:
    items = balance.search(logs.date == d)
    for i in items:
        dates.append(i.get('date'))
        if i.get('change')=='expense': 
            amount.append(i.get('amount')*(-1))
        else: 
            amount.append(i.get('amount'))

ydata = []
curAm = 0
for i in amount:
    curAm += i
    ydata.append(curAm)

fig, ax = plt.subplots(facecolor=('#031926'))
ax.set_facecolor('#031926')
ax.tick_params(labelcolor='#F0F2EF')
ax.set_title('Expense tracker', color='#F0F2EF')
ax.set_xlabel('Dates', color='#F0F2EF')
ax.set_ylabel('Balance', color='#F0F2EF')
ax.grid(True)
plt.plot(dates, ydata, color='#C33149')
plt.show()